<?php
/**
 * This file is the entry point to the zipcourse module.
 *
 * @package   local_zipcourse
 * @copyright 2019 Pratik Sontakke and Beejal Oswal
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
if(!is_dir('../../temp')){
    mkdir('../../temp');
}

require_once('./lib.php');
$courseid = required_param('id', PARAM_INT);

if(!is_dir(DOWNLOAD_DIR)){
    mkdir(DOWNLOAD_DIR);
}

$obj = new zipcourse($courseid);

$obj->create_and_download();

rmrf(DOWNLOAD_DIR);
/*
* source: https://gist.github.com/irazasyed/4340722
*/
function rmrf($dir) {
    foreach (glob($dir) as $file) {
        if (is_dir($file)) {
            rmrf("$file/*");
            rmdir($file);
        } else {
            unlink($file);
        }
    }
}
