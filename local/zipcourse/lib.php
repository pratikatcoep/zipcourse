<?php
/**
 * Main class and all essential functions to assist local/zipcourse.index.php.
 *
 * @package   local_zipcourse
 * @copyright 2019 Pratik Sontakke and Beejal Oswal
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 define('DOWNLOAD_DIR', '../../temp/Whole_course');

 require_once('../../config.php');

 /*
 * This class zipcourse has a method for each course module
 * These methods are private, To use them individually make them public
 * It contains code to zip the Directory
 * It also contains download code for the sake of my module motive
 * it's public
 */
 class zipcourse{
     /*$courseid is id of course which will be used thoughout the class*/
     private $courseid;


     public function __construct($courseid){
         $this->courseid = $courseid;
     }

     /*
     * @param $path is file name with actual path
     * @param $string is string which usually contains html code
     * Function is used to create $path.html from $string
     * Mostly used for writing description of course module into file
     * and storing them in proper directory so that it can be used for ziping
     * .html is obvious as browser interprets text as well as html
     */
     private function create_html_from_string($path, $string){
         if(strlen($string) == 0){
             return;
         }
         $new_file = fopen($path, 'a+');
         fwrite($new_file, $string);
         fclose($new_file);
     }
     /*Tranfer all stuff belogning to each assignment to temp for particular course*/
     private function load_assignment(){
         global $DB, $USER;
         if (!is_dir(DOWNLOAD_DIR.'/Assignments')) {
             mkdir(DOWNLOAD_DIR.'/Assignments');
         }

         $assigns = $DB->get_records_sql('SELECT * FROM {assign} WHERE course = ?', [$this->courseid]);
         $assignids = Array();
         $assignnames = Array();
         $assignintros = Array();

         foreach($assigns as $x){
             array_push($assignids, $x->id);
             array_push($assignnames, $x->name);
             array_push($assignintros, $x->intro);

         }
         //$contexts contains all contexts regarding to every assignment in a paraticular course
         $contexts = Array();
         foreach($assignids as $x){
             /*Here We get contexts for each Assignment*/

             $cm = get_coursemodule_from_instance('assign', $x, $this->courseid);
             if($cm->deletioninprogress == '0'){
                 array_push($contexts, get_context_instance(CONTEXT_MODULE, $cm->id));
             }
             else {
                 //course module is not available anymore
                 array_push($contexts, -1);
             }
         }

         $fs = get_file_storage();
         $i = 0;
         foreach($contexts as $x){
             if($x == -1){
                 $i++;
                 continue;
             }
             $curr_par_dir = DOWNLOAD_DIR.'/Assignments'.'/'.$assignnames[$i];
             if(!is_dir($curr_par_dir)){
                 mkdir($curr_par_dir);
             }


             $this->create_html_from_string($curr_par_dir."/description.html", $assignintros[$i]);


             $files_table = $DB->get_records_sql('SELECT * FROM {files} WHERE contextid =
                 ? and author <> ? and (filearea = ? or (filearea = ? and userid = ?))',
                  Array($x->id, 'NULL', 'introattachment', 'submission_files', $USER->id));
             foreach($files_table as $key => $y){
                 if(strcmp($y->filearea, 'introattachment') == 0){
                    $curr_child_dir = $curr_par_dir.'/attachedbyteacher';
                 }
                 else if(strcmp($y->filearea, 'submission_files') == 0){
                     /*user validation code*/
                     $curr_child_dir = $curr_par_dir.'/submittedbyyou';
                 }
                 if(!is_dir($curr_child_dir)){
                     mkdir($curr_child_dir);
                 }
                 $fileinfo = array(
                     'component' => $y->component,
                     'filearea' => $y->filearea,
                     'itemid' => $y->itemid,
                     'contextid' => $y->contextid,
                     'filepath' => $y->filepath,
                     'filename' => $y->filename);
                 $file = $fs->get_file((int)$fileinfo['contextid'], $fileinfo['component'], $fileinfo['filearea'],
                                       (int)$fileinfo['itemid'], $fileinfo['filepath'], $fileinfo['filename']);
                 if($file){
                     $file->copy_content_to($curr_child_dir.'/'.$fileinfo['filename']);
                 }

             }
             $i++;
         }

     }
     /*
     * @param $arg contains all mdl_files table within right contexts
     * @param $path contains path where all work will be done
     * It creates all empty folders with the help of $arg->$filePath
     * so that it will be easier for load_folder function to transfer file to
     * exact path and eventually transfer folder.
     */
     private function create_folders_for_mod_folder($arg, $path){
         foreach($arg as $key => $y){
             $current = $path;
             $token = strtok($y->filepath, "/");
             while($token !== false){
                 $current = $current.'/'.$token;
                 if(!is_dir($current)){
                     mkdir($current);
                 }
                 $token = strtok("/");

             }
         }
     }

     /*
     * It creates folders  in ../../temp/whole_course/Folders
     * which have been uploaded in course
     */
     private function load_folder(){
         global $DB, $USER;
         if (!is_dir(DOWNLOAD_DIR.'/Folders')) {
             mkdir(DOWNLOAD_DIR.'/Folders');
         }
         $folders = $DB->get_records_sql('SELECT * FROM {folder} WHERE course = ?', [$this->courseid]);

         $folderintros = Array();
         $folderids = Array();
         $foldernames = Array();
         foreach($folders as $x){
             array_push($folderids, $x->id);
             array_push($foldernames, $x->name);
             array_push($folderintros, $x->intro);
         }
         $contexts = Array();

         foreach($folderids as $x){
             /*Here We get contexts for each Folder*/
             $cm = get_coursemodule_from_instance('folder', $x, $this->courseid);
             if($cm->deletioninprogress == '0'){
                 array_push($contexts, get_context_instance(CONTEXT_MODULE, $cm->id));
             }
             else {
                 array_push($contexts, -1);
             }
         }
         $fs = get_file_storage();
         $i = 0;
         foreach($contexts as $x){
             if($x == -1){
                 $i++;
                 continue;
             }
             $curr_par_dir = DOWNLOAD_DIR.'/Folders'.'/'.$foldernames[$i];
             if(!is_dir($curr_par_dir)){
                 mkdir($curr_par_dir);
             }
             $this->create_html_from_string($curr_par_dir."/description.html", $folderintros[$i]);

             $files_table = $DB->get_records_sql('SELECT * FROM {files} WHERE contextid =
                 ? and author <> ? and filearea = ?', Array($x->id, 'NULL', 'content'));
             $this->create_folders_for_mod_folder($files_table, $curr_par_dir);
             foreach($files_table as $key => $y){
                 $fileinfo = array(
                     'component' => $y->component,
                     'filearea' => $y->filearea,
                     'itemid' => $y->itemid,
                     'contextid' => $y->contextid,
                     'filepath' => $y->filepath,
                     'filename' => $y->filename);
                 $file = $fs->get_file((int)$fileinfo['contextid'], $fileinfo['component'], $fileinfo['filearea'],
                                       (int)$fileinfo['itemid'], $fileinfo['filepath'], $fileinfo['filename']);
                 if($file){
                     $file->copy_content_to($curr_par_dir.$fileinfo['filepath'].$fileinfo['filename']);
                 }
             }
             $i++;
         }

     }
     /*
     * All resources or files are transferred to ../../temp/whole_course/Resources
     */
     private function load_resource(){
         global $DB, $USER;
         if (!is_dir(DOWNLOAD_DIR.'/Resources')) {
             mkdir(DOWNLOAD_DIR.'/Resources');
         }

         $resources = $DB->get_records_sql('SELECT * FROM {resource} WHERE course = ?', [$this->courseid]);
         $resourceids = Array();
         foreach($resources as $x){
             array_push($resourceids, $x->id);
         }
         //$contexts contains all contexts regarding to every resourse in a paraticular course
         $contexts = Array();
         foreach($resourceids as $x){
             /*Here We get contexts for each Resource*/

             $cm = get_coursemodule_from_instance('resource', $x, $this->courseid);
             if($cm->deletioninprogress == '0'){
                 array_push($contexts, get_context_instance(CONTEXT_MODULE, $cm->id));
             }
             else {
                 //course module is not available anymore
                 array_push($contexts, -1);
             }
         }
         $fs = get_file_storage();
         $i = 0;
         foreach($contexts as $x){
             if($x == -1){
                 $i++;
                 continue;
             }

             $files_table = $DB->get_records_sql('SELECT * FROM {files} WHERE contextid =
                 ? and author <> ? and filearea = ?',
                  Array($x->id, 'NULL', 'content'));
             foreach($files_table as $key => $y){

                 $fileinfo = array(
                     'component' => $y->component,
                     'filearea' => $y->filearea,
                     'itemid' => $y->itemid,
                     'contextid' => $y->contextid,
                     'filepath' => $y->filepath,
                     'filename' => $y->filename);
                 $file = $fs->get_file((int)$fileinfo['contextid'], $fileinfo['component'], $fileinfo['filearea'],
                                       (int)$fileinfo['itemid'], $fileinfo['filepath'], $fileinfo['filename']);
                 if($file){
                     /*Work here add file to $curr_child_dir*/
                     $file->copy_content_to(DOWNLOAD_DIR.'/Resources'.'/'.$fileinfo['filename']);
                 }

             }
             $i++;
         }

     }
     /*
     * All pages are written in html file and transferred to
     * ../../temp/whole_course/Pages/
     * name of the file is name of module
     */
     private function load_page(){
         global $DB, $USER;
         if (!is_dir(DOWNLOAD_DIR.'/Pages')) {
             mkdir(DOWNLOAD_DIR.'/Pages');
         }

         $pages = $DB->get_records_sql('SELECT * FROM {page} WHERE course = ?', [$this->courseid]);
         $pageids = Array();
         $pagenames = Array();
         $pagecontents = Array();

         foreach($pages as $x){
             array_push($pageids, $x->id);
             array_push($pagenames, $x->name);
             array_push($pagecontents, $x->content);
         }
         $curr_par_dir = DOWNLOAD_DIR.'/Pages'.'/';
         $i = 0;
         foreach($pageids as $x){
             $cm = get_coursemodule_from_instance('page', $x, $this->courseid);
             if($cm->deletioninprogress == '1'){
                 $i++;
                 continue;
             }
             $this->create_html_from_string($curr_par_dir.$pagenames[$i].'.html', $pagecontents[$i]);
             $i++;
         }
     }

     /*The code is referenced from stackoverflow
     * @param $path is source of the directory, typically, temp/Whole_course
     * @param $target is target file name
     */
     private function zipIt($path, $target){
         $rootPath = realpath($path);

         $zip = new ZipArchive();
         $zip->open($target, ZipArchive::CREATE | ZipArchive::OVERWRITE);

         /** @var SplFileInfo[] $files */
         $files = new RecursiveIteratorIterator(
             new RecursiveDirectoryIterator($rootPath),
             RecursiveIteratorIterator::LEAVES_ONLY
         );
         foreach ($files as $name => $file)
         {
             if (!$file->isDir())
             {
                 $filePath = $file->getRealPath();
                 $relativePath = substr($filePath, strlen($rootPath) + 1);
                 $zip->addFile($filePath, $relativePath);
             }
         }
         $zip->close();

     }

     /*
     * This function invokes all other functions in this class
     * so that all functions are private except this one
     * it downloads zip archieve stored in ../../temp/course.zip
     * It is used in ./index.php
     */
     public function create_and_download(){
         $archive_file_name = '../../temp/course.zip';
         $this->load_assignment();
         $this->load_folder();
         $this->load_resource();
         $this->load_page();
         $this->zipIt(DOWNLOAD_DIR, $archive_file_name);
         header("Content-type: application/zip");
         header("Content-Disposition: attachment; filename=course.zip");
         header("Pragma: no-cache");
         header("Expires: 0");
         readfile("$archive_file_name");
         unlink($archive_file_name);
     }

 }
